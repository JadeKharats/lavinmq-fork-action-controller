require "./lavinmq/version"
require "./stdlib/*"
require "./lavinmq/config"
require "./lavinmq/server_cli"
require "./lavinmq/living_server"

config = LavinMQ::Config.instance
LavinMQ::ServerCLI.new(config).parse

{% unless flag?(:gc_none) %}
  if config.raise_gc_warn?
    LibGC.set_warn_proc ->(msg, _word) {
      raise String.new(msg)
    }
  end
{% end %}

# config has to be loaded before we require vhost/queue, byte_format is a constant
require "./lavinmq/launcher"
require "./lavinmq/replication/client"
if uri = config.replication_follow
  begin
    LavinMQ::Replication::Client.new(config.data_dir).follow(uri)
  rescue ex : ArgumentError
    abort ex.message
  end
else
  living_server = LivingServers.instance
  LavinMQ::Launcher.new(living_server).run # will block
end
