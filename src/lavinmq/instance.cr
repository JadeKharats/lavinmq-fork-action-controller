class Singleton
  @@instance : Singleton = self.new

  def self.instance : Singleton
    @@instance
  end

  private def initialize
  end

  def add_amqp_server(server : LavinMQ::Server)
    @amqp_server = server
  end

  def amqp_server
    @amqp_server
  end
end
