require "./server"
require "./http/http_server"
require "./api/api_server"

class LivingServers
  getter amqp_server : LavinMQ::Server
  getter http_server : LavinMQ::HTTP::Server
  getter api_server : ApiServer
  getter config : LavinMQ::Config

  private def initialize
    @config = LavinMQ::Config.instance
    @amqp_server = LavinMQ::Server.new(@config.data_dir)
    @http_server = LavinMQ::HTTP::Server.new(@amqp_server)
    @api_server = ApiServer.new(@config.http_api_port, @config.http_bind)
  end

  def self.instance
    @@instance ||= new
  end

  def stop
    @api_server.put_on_stand_by
    # @http_server.close
    @amqp_server.stop
  end

  def start
    @amqp_server.start
    # i should to exit from the launcher the method to start the http server
    # @http_server.start
    @api_server.wake_up
  end
end
