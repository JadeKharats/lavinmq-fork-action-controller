class Whoami < Controller
  base "/api/whoami"

  @[AC::Route::GET("/")]
  def whoami : UserController::User
    if user = @current_user
      UserController::User.from_lavin_user(user)
    else
      raise LavinMQ::Error::Unauthorized.new("You should use login/password")
    end
  end
end
