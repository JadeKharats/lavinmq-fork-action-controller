class VhostController < Controller
  base "/api/vhosts"
  before_action :refuse_unless_administrator, except: [:list_all_vhosts, :show_vhost]

  # List all vhosts
  # List all vhosts in the server.
  @[AC::Route::GET("/")]
  def list_all_vhosts : Array(Vhost)
    vhosts = Array(Vhost).new
    vhosts(@current_user.not_nil!).each do |vhost|
      vhosts << Vhost.from_store(vhost)
    end
    vhosts
  end

  # List vhost
  # Find a specific vhost by name.
  @[AC::Route::GET("/:name")]
  def show_vhost(name : String) : Vhost
    if @amqp_server.vhosts[name]?
      Vhost.from_store(@amqp_server.vhosts[name])
    else
      raise LavinMQ::Error::NotFound.new("Vhost #{name} not found")
    end
  end

  # Create/update vhost
  # Create a new vhost, or update an existing vhost.
  @[AC::Route::PUT("/:name", status: {
    LavinMQ::VHost => HTTP::Status::CREATED,
    Nil            => HTTP::Status::NO_CONTENT,
  })]
  def create_or_update(name : String) : Nil | LavinMQ::VHost
    vhost_response = nil
    unless @amqp_server.vhosts[name]?
      vhost_response = @amqp_server.vhosts.create(name, @current_user.not_nil!)
    end
    vhost_response
  end

  # Delete vhost
  # Remove a specific vhost by name.
  @[AC::Route::DELETE("/:name", status_code: HTTP::Status::NO_CONTENT)]
  def delete(name : String)
    if @amqp_server.vhosts[name]?
      @amqp_server.vhosts.delete(name)
    else
      raise LavinMQ::Error::NotFound.new("Vhost #{name} not found")
    end
  end

  # List permissions of vhost
  # List the permissions for a vhost.
  @[AC::Route::GET("/:vhost_name/permissions")]
  def permissions(vhost_name : String) : Array(UserController::Permission)
    permissions_list = Array(UserController::Permission).new
    if @amqp_server.vhosts[vhost_name]?
      @amqp_server.users.each do |_, u|
        unless u.hidden?
          if permission = u.permissions[vhost_name]?.try { |p| u.permissions_details(vhost_name, p) }
            permissions_list << UserController::Permission.new(u.name, vhost_name, permission[:configure], permission[:read], permission[:write])
          end
        end
      end
    else
      raise LavinMQ::Error::NotFound.new("Vhost #{vhost_name} not found")
    end
    permissions_list
  end

  # Purge and Close consumers
  # Purge and close all consumers
  @[AC::Route::POST("/:vhost_name/purge_and_close_consumers", body: :purge_params, status_code: HTTP::Status::NO_CONTENT)]
  def purge_and_close_consumers(vhost_name : String, purge_params : PurgeParams)
    raise LavinMQ::Error::NotFound.new("Vhost #{vhost_name} not found") unless @amqp_server.vhosts[vhost_name]?
    purge_params.check_dir_name
    vhost = @amqp_server.vhosts[vhost_name]
    vhost.purge_queues_and_close_consumers(purge_params.backup?, purge_params.backup_dir_name)
  end

  class PurgeParams
    include JSON::Serializable
    property? backup : Bool = true
    property backup_dir_name : String = Time.utc.to_unix.to_s

    def check_dir_name
      unless @backup_dir_name.match(/^[a-z0-9]+$/)
        raise LavinMQ::Error::BadRequest.new("Bad backup dir name, use only lower case letters and numbers")
      end
    end
  end

  class Vhost
    include JSON::Serializable
    property name : String
    property dir : String
    property? tracing : Bool
    property cluster_state : Hash(Symbol, String | UInt64)
    property messages : UInt64
    property messages_unacknowledged : UInt64
    property messages_ready : UInt64
    property message_stats : MessageStats

    def initialize(@name, @dir, @tracing, @cluster_state, @messages, @messages_unacknowledged, @messages_ready, @message_stats)
    end

    def self.from_store(vhost : LavinMQ::VHost)
      details = vhost.message_details
      self.new(
        vhost.name,
        vhost.dir,
        false,
        Hash(Symbol, String | UInt64).new,
        details["messages"],
        details["messages_unacknowledged"],
        details["messages_ready"],
        MessageStats.from_details(details["message_stats"])
      )
    end
  end

  class MessageStats
    include JSON::Serializable
    property ack : UInt64
    property confirm : UInt64
    property deliver : UInt64
    property get : UInt64
    property get_no_ack : UInt64
    property publish : UInt64
    property redeliver : UInt64
    property return_unroutable : UInt64

    def initialize(@ack, @confirm, @deliver, @get, @get_no_ack, @publish, @redeliver, @return_unroutable)
    end

    def self.from_details(details)
      self.new(
        details["ack"],
        details["confirm"],
        details["deliver"],
        details["get"],
        details["get_no_ack"],
        details["publish"],
        details["redeliver"],
        details["return_unroutable"]
      )
    end
  end
end
