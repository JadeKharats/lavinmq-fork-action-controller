class StatusController < Controller
  base "/api/overview"

  OVERVIEW_STATS = {"ack", "deliver", "get", "publish", "confirm", "redeliver", "reject"}
  EXCHANGE_TYPES = {"direct", "fanout", "topic", "headers", "x-federation-upstream", "x-consistent-hash"}
  CHURN_STATS    = {"connection_created", "connection_closed", "channel_created", "channel_closed", "queue_declared", "queue_deleted"}

  @[AC::Route::GET("/")]
  def index : Overview
    raise LavinMQ::Error::Unauthorized.new("You don't use a valid user") if @current_user.nil?
    Overview.new(context)
  end

  class Overview < Interface
    include JSON::Serializable
    EXCHANGE_TYPES = ["direct", "fanout", "topic", "headers", "x-federation-upstream", "x-consistent-hash"]
    property lavinmq_version : String = LavinMQ::VERSION
    property node : String = System.hostname
    property uptime : Int64
    property object_totals : ObjectTotals?
    property queue_totals : QueueTotals?
    property listeners : Array(Listener)
    property exchange_types : Array(ExchangesTypes)
    property message_stats : MessageStats?

    def initialize(context : HTTP::Server::Context)
      @uptime = amqp_server.uptime.to_i
      @exchange_types = EXCHANGE_TYPES.map { |name| ExchangesTypes.new(name) }
      @listeners = Array(Listener).new
      amqp_server.listeners.each do |listener|
        @listeners << Listener.new(listener)
      end
      user = nil
      if username = context.authenticated_username?
        user = amqp_server.users[username]
      end
      if user
        @object_totals = ObjectTotals.new(user)
        @queue_totals = QueueTotals.new(user)
        @message_stats = MessageStats.new(user)
      end
    end
  end

  class Listener < Interface
    include JSON::Serializable
    property path : String?
    property protocol : Symbol
    property ip_address : String?
    property port : Int32?

    def initialize(listener : NamedTuple(ip_address: String, protocol: Symbol, port: Int32))
      @ip_address = listener[:ip_address]
      @protocol = listener[:protocol]
      @port = listener[:port]
    end

    def initialize(listener : NamedTuple(path: String, protocol: Symbol))
      @protocol = listener[:protocol]
      @path = listener[:path]
    end
  end

  class ExchangesTypes < Interface
    include JSON::Serializable
    property name : String

    def initialize(@name)
    end
  end

  class MessageStats < Interface
    include JSON::Serializable
    OVERVIEW_STATS = {"ack", "deliver", "get", "publish", "confirm", "redeliver", "reject"}
    {% for name in OVERVIEW_STATS %}
      property {{name.id}} : UInt64 = 0_u64
      property {{name.id}}_details : Details = Details.new(0_f64, Deque(Float64).new(LavinMQ::Config.instance.stats_log_size))
    {% end %}

    def initialize(user : LavinMQ::User)
      vhosts(user).each do |vhost|
        vhost_stats_details = vhost.stats_details
        {% for name in OVERVIEW_STATS %}
          @{{name.id}} += vhost_stats_details[:{{name.id}}]
          @{{name.id}}_details.rate += vhost_stats_details[:{{name.id}}_details][:rate]
          add_logs!(@{{name.id}}_details.log, vhost_stats_details[:{{name.id}}_details][:log])
        {% end %}
      end
    end
  end

  class Details < Interface
    include JSON::Serializable
    property rate : Float64
    property log : Deque(Float64)

    def initialize(@rate, @log)
    end
  end

  class QueueTotals < Interface
    include JSON::Serializable
    property messages : UInt32 = 0
    property messages_ready : UInt32 = 0
    property messages_unacknowledged : UInt32 = 0
    property messages_log : Deque(UInt32)
    property messages_ready_log : Deque(UInt32)
    property messages_unacknowledged_log : Deque(UInt32)

    def initialize(user : LavinMQ::User)
      ready, unacked = 0_u32, 0_u32
      ready_log = Deque(UInt32).new(LavinMQ::Config.instance.stats_log_size)
      unacked_log = Deque(UInt32).new(LavinMQ::Config.instance.stats_log_size)
      vhosts(user).each do |vhost|
        vhost.queues.each_value do |queue|
          ready += queue.message_count
          unacked += queue.unacked_count
          add_logs!(ready_log, queue.message_count_log)
          add_logs!(unacked_log, queue.unacked_count_log)
        end
      end
      @messages = ready + unacked
      @messages_ready = ready
      @messages_unacknowledged = unacked
      @messages_log = add_logs(ready_log, unacked_log)
      @messages_ready_log = ready_log
      @messages_unacknowledged_log = unacked_log
    end
  end

  class ObjectTotals < Interface
    include JSON::Serializable
    property channels : UInt32 = 0
    property connections : UInt32 = 0
    property consumers : UInt32 = 0
    property exchanges : UInt32 = 0
    property queues : UInt32 = 0

    def initialize(user : LavinMQ::User)
      vhosts(user).each do |vhost|
        vhost.connections.each do |c|
          @connections += 1
          @channels += c.channels.size
          @consumers += c.channels.each_value.sum &.consumers.size
        end
        @exchanges += vhost.exchanges.size
        @queues += vhost.queues.size
      end
    end
  end
end
