class Running < Controller
  base "/api/server"
  before_action :refuse_unless_administrator

  @[AC::Route::GET("/stop")]
  def stop_app
    LivingServers.instance.stop
  end

  @[AC::Route::GET("/start")]
  def start_app
    LivingServers.instance.start
  end
end
