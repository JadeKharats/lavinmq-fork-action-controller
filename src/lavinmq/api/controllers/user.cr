class UserController < Controller
  base "/api/users"
  before_action :refuse_unless_administrator

  # List all users
  # List all users in the server.
  @[AC::Route::GET("/")]
  def list_all_users : Array(User)
    list_users = Array(User).new
    @amqp_server.users.each do |_username, user|
      list_users << User.from_lavin_user(user) unless user.hidden?
    end
    render json: list_users
  end

  # List users with no permissions
  # List all users that doesn't have any permissions.
  @[AC::Route::GET("/without-permissions")]
  def list_all_users_without_permissions : Array(User)
    list_users = Array(User).new
    @amqp_server.users.each_value.reject(&.hidden?).select(&.permissions.empty?).each do |user|
      list_users << User.from_lavin_user(user)
    end
    render json: list_users
  end

  # Bulk delete users
  # Delete multiple users.
  @[AC::Route::POST("/bulk-delete", body: :list_users, status_code: HTTP::Status::NO_CONTENT)]
  def bulk_delete(list_users : BulkUser)
    LavinMQ::Error::BadRequest.new("Field 'users' is required") if list_users.users.empty?
    list_users.users.each do |user|
      @amqp_server.users.delete(user, false)
    end
  end

  # List user
  # Find a specific user by name.
  @[AC::Route::GET("/:name")]
  def list_user(name : String) : User?
    if user = @amqp_server.users[name]
      render json: User.from_lavin_user(user)
    end
  end

  # Create/update user
  # Create new user, or update an existing user.
  @[AC::Route::PUT("/:name", body: :user, status: {
    LavinMQ::User => HTTP::Status::CREATED,
    Nil           => HTTP::Status::NO_CONTENT,
  })]
  def create_or_update(name : String, user : UserInput) : Nil | LavinMQ::User
    user_response = nil
    LavinMQ::Error::BadRequest.new("Illegal user name") if LavinMQ::UserStore.hidden?(name)
    unless @amqp_server.flow?
      LavinMQ::Error::PreconditionFailed.new("Server low on disk space, can not create new user")
    end
    hashing_algorithm = user.hashing_algorithm || "SHA256"
    tags = Array(LavinMQ::Tag).new
    if tags_list = user.tags
      tags = LavinMQ::Tag.parse_list(tags_list).uniq
    end
    if stored_user = @amqp_server.users[name]?
      if ph = user.password_hash
        stored_user.update_password_hash(ph, hashing_algorithm)
      elsif pwd = user.password
        stored_user.update_password(pwd)
      end
      unless user.tags.nil?
        stored_user.tags = tags
      end
      @amqp_server.users.save!
    else
      if ph = user.password_hash
        @amqp_server.users.add(name, ph, hashing_algorithm, tags)
        user_response = @amqp_server.users[name]
      elsif pwd = user.password
        @amqp_server.users.create(name, pwd, tags)
        user_response = @amqp_server.users[name]
      else
        LavinMQ::Error::BadRequest.new("Field 'password_hash' or 'password' is required when creating new user")
      end
    end
    user_response
  end

  # Delete user
  # Remove a specific user by name.
  @[AC::Route::DELETE("/:name", status_code: HTTP::Status::NO_CONTENT)]
  def delete(name : String)
    @amqp_server.users.delete(name)
  end

  # List permissions of user
  # List permissions by user.
  @[AC::Route::GET("/:name/permissions")]
  def permissions(
    name : String,
    @[AC::Param::Info(description: "VHOST", example: "vhost", required: false)] vhost : String?
  ) : Array(Permission)
    stored_user = @amqp_server.users[name]
    permissions_list = Array(Permission).new
    stored_permissions = stored_user.permissions
    if vh = vhost
      permission = stored_permissions[vh]
      permissions_list << Permission.new(name, vh, permission[:config], permission[:read], permission[:write])
    else
      stored_permissions.each do |vhost_perm, perm|
        permissions_list << Permission.new(name, vhost_perm, perm[:config], perm[:read], perm[:write])
      end
    end
    render json: permissions_list
  end

  class UserInput
    include JSON::Serializable
    property tags : String?
    property hashing_algorithm : String?
    property password_hash : String?
    property password : String?
  end

  class Permission
    include JSON::Serializable
    property user : String
    property vhost : String
    property configure : Regex
    property read : Regex
    property write : Regex

    def initialize(@user, @vhost, @configure, @read, @write)
    end
  end

  class BulkUser
    include JSON::Serializable
    property users : Array(String)
  end

  class User
    include JSON::Serializable
    property name : String
    property password_hash : String
    property hashing_algorithm : String
    property tags : String

    def initialize(@name, @password_hash, @hashing_algorithm, @tags)
    end

    def self.from_lavin_user(user : LavinMQ::User)
      self.new(
        user.name,
        user.password.to_s,
        user.password.not_nil!.hash_algorithm,
        user.tags.join(",")
      )
    end
  end
end
