require "json"

abstract class Interface
  def amqp_server
    LivingServers.instance.amqp_server
  end

  def vhosts(user : LavinMQ::User)
    amqp_server.vhosts.each_value.select do |v|
      full_view_vhosts_access = user.tags.any? { |t| t.administrator? || t.monitoring? }
      amqp_access = user.permissions.has_key?(v.name)
      full_view_vhosts_access || (amqp_access && !user.tags.empty?)
    end
  end

  def add_logs!(logs_a, logs_b)
    until logs_a.size >= logs_b.size
      logs_a.unshift 0
    end
    until logs_b.size >= logs_a.size
      logs_b.unshift 0
    end
    logs_a.size.times do |i|
      logs_a[i] += logs_b[i]
    end
    logs_a
  end

  private def add_logs(logs_a, logs_b)
    add_logs!(logs_a.dup, logs_b)
  end
end
