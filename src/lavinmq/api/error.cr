require "json"

class LavinMQ::Error < Exception
  class Unauthorized < LavinMQ::Error; end

  class Forbidden < LavinMQ::Error; end

  class PreconditionFailed < LavinMQ::Error; end

  class BadRequest < LavinMQ::Error; end

  class UnavailableAMQPServer < LavinMQ::Error; end

  class NilPassword < LavinMQ::Error; end

  class NotFound < LavinMQ::Error; end

  class ServiceUnavailable < LavinMQ::Error; end

  struct CommonResponse
    include JSON::Serializable

    getter error : String?
    getter reason : String?

    def initialize(@error, @reason)
    end
  end
end
