require "action-controller"
require "base64"
require "../living_server"
require "./error"
require "./manage_auth"
require "./manage_vhost"

abstract class Controller < ActionController::Base
  include LavinMQ::API::ManageAuth
  include LavinMQ::API::ManageVhost
  @current_user : LavinMQ::User?
  @amqp_server : LavinMQ::Server = LivingServers.instance.amqp_server

  before_action :api_server_is_open, except: [:start_app]
  before_action :amqpserver_is_open, except: [:start_app]
  before_action :enable_cors
  before_action :auth_user

  def amqpserver_is_open
    raise LavinMQ::Error::PreconditionFailed.new("AMQP server is closed, retry later") if @amqp_server.closed?
  end

  def api_server_is_open
    raise LavinMQ::Error::ServiceUnavailable.new("server standby") if LivingServers.instance.api_server.stand_by?
  end

  def enable_cors
    response.headers["Access-Control-Allow-Origin"] = "*"
    response.headers["Access-Control-Allow-Headers"] = "Content-Type"
    response.headers["Content-Type"] = "application/json"
    response.headers["Access-Control-Allow-Methods"] = "GET,HEAD,POST,DELETE,OPTIONS,PUT,PATCH"
  end

  @[AC::Route::Exception(LavinMQ::Error::NotFound, status_code: HTTP::Status::NOT_FOUND)]
  def not_found(error) : LavinMQ::Error::CommonResponse
    LavinMQ::Error::CommonResponse.new("ressource not found", error.message)
  end

  @[AC::Route::Exception(LavinMQ::Error::Forbidden, status_code: HTTP::Status::SERVICE_UNAVAILABLE)]
  @[AC::Route::Exception(LavinMQ::Error::ServiceUnavailable, status_code: HTTP::Status::SERVICE_UNAVAILABLE)]
  def service_unavailable(error) : LavinMQ::Error::CommonResponse
    LavinMQ::Error::CommonResponse.new("service unavailable", error.message)
  end

  @[AC::Route::Exception(LavinMQ::Error::Forbidden, status_code: HTTP::Status::FORBIDDEN)]
  def forbidden_return(error) : LavinMQ::Error::CommonResponse
    LavinMQ::Error::CommonResponse.new("forbidden", error.message)
  end

  @[AC::Route::Exception(LavinMQ::Error::NilPassword, status_code: HTTP::Status::UNAUTHORIZED)]
  @[AC::Route::Exception(LavinMQ::Error::Unauthorized, status_code: HTTP::Status::UNAUTHORIZED)]
  def unauthorized_return(error) : LavinMQ::Error::CommonResponse
    LavinMQ::Error::CommonResponse.new("unauthorized", error.message)
  end

  @[AC::Route::Exception(LavinMQ::Error::PreconditionFailed, status_code: HTTP::Status::PRECONDITION_FAILED)]
  def precondition_failed_return(error) : LavinMQ::Error::CommonResponse
    LavinMQ::Error::CommonResponse.new("Precondition failed", error.message)
  end

  @[AC::Route::Exception(LavinMQ::Error::BadRequest, status_code: HTTP::Status::BAD_REQUEST)]
  def bad_request_return(error) : LavinMQ::Error::CommonResponse
    LavinMQ::Error::CommonResponse.new("bad_request", error.message)
  end

  @[AC::Route::Exception(JSON::SerializableError, status_code: HTTP::Status::BAD_REQUEST)]
  def json_serializable_error(error) : LavinMQ::Error::CommonResponse
    LavinMQ::Error::CommonResponse.new("bad_request", "Input needs to be a JSON object.")
  end

  @[AC::Route::Exception(JSON::ParseException, status_code: HTTP::Status::BAD_REQUEST)]
  def json_mal_formatted(error) : LavinMQ::Error::CommonResponse
    LavinMQ::Error::CommonResponse.new("bad_request", "Malformed JSON.")
  end

  @[AC::Route::Exception(AC::Route::NotAcceptable, status_code: HTTP::Status::NOT_ACCEPTABLE)]
  @[AC::Route::Exception(AC::Route::UnsupportedMediaType, status_code: HTTP::Status::UNSUPPORTED_MEDIA_TYPE)]
  def bad_media_type(error) : AC::Error::ContentResponse
    AC::Error::ContentResponse.new error: error.message.as(String), accepts: error.accepts
  end

  @[AC::Route::Exception(AC::Route::Param::MissingError, status_code: HTTP::Status::UNPROCESSABLE_ENTITY)]
  def invalid_param(error) : AC::Error::ParameterResponse
    AC::Error::ParameterResponse.new error: error.message.as(String), parameter: error.parameter, restriction: error.restriction
  end
end
