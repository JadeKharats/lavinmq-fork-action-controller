require "./interface"
require "./controller"
require "./controllers/*"
require "action-controller/server"

class ApiServer < ActionController::Server
  getter? stand_by : Bool = false

  def put_on_stand_by
    @stand_by = true
  end

  def wake_up
    @stand_by = false
  end
end
