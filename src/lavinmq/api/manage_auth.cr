module LavinMQ::API::ManageAuth
  private def refuse_unless_administrator
    if context_user = @current_user
      unless context_user.tags.any? &.administrator?
        Log.warn { "user=#{context_user.name} does not have administrator access" }
        access_refused
      end
    else
      access_refused
    end
  end

  private def access_refused
    raise LavinMQ::Error::Forbidden.new("Access refused")
  end

  def auth_user
    if auth = basic_auth(context)
      username, password = auth
      if valid_auth?(username, password, context.request.remote_address)
        @current_user = @amqp_server.users[username]
      end
    end
    raise LavinMQ::Error::Forbidden.new("Access refused") if @current_user.nil?
  end

  private def valid_auth?(username, password, remote_address) : Bool
    return false if password.empty?
    if user = @amqp_server.users[username]?
      if user_password = user.password
        if user_password.verify(password)
          if guest_only_loopback?(remote_address, username)
            return false if user.tags.empty?
            return true
          end
        else
          raise LavinMQ::Error::Unauthorized.new("Invalid password")
        end
      end
    else
      raise LavinMQ::Error::Unauthorized.new("This user does not exist")
    end
    false
  end

  private def guest_only_loopback?(remote_address, username) : Bool
    return true unless LavinMQ::Config.instance.guest_only_loopback?
    return true unless username == "guest"
    case remote_address
    when Socket::IPAddress   then remote_address.loopback?
    when Socket::UNIXAddress then true
    else                          false
    end
  end

  private def internal_unix_socket?(context) : Bool
    false
  end

  private def basic_auth(context)
    if auth = context.request.headers["Authorization"]?
      if auth.starts_with? "Basic "
        base64 = auth[6..]
        return decode(base64)
      end
    else
      raise LavinMQ::Error::Unauthorized.new("Header Authorization is required")
    end
  end

  private def unauthenticated(context)
    context.response.status_code = 401
  end

  private def decode(base64) : Tuple(String, String)?
    string = Base64.decode_string(base64)
    if idx = string.index(':')
      username = string[0...idx]
      password = string[idx + 1..]
      raise LavinMQ::Error::NilPassword.new("password is required") if password.empty?
      return {username, password}
    end
  rescue Base64::Error
  end
end
