require "./config"
require "./version"
require "./api/api_server"
require "./user"

docs = ActionController::OpenAPI.generate_open_api_docs(
  title: "Application",
  version: "0.0.1",
  description: "App description for OpenAPI docs"
).to_yaml

puts docs
